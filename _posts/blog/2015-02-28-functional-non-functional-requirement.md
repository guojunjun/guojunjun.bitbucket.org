---
layout: post
title: "Functional and nonfunctional requirement"
modified:
categories: blog
excerpt: "Different between functional and nonfunctional requirement"
tags: [software, Software development, requirement]
image:
  feature: summer2014abug.jpg
  credit: GuoJunjun
  creditlink: https://www.flickr.com/photos/guojunjun/
date: 2015-02-28
comments: true
share: true
---

Software requirements are simply the needs of stakeholders that will be solved by software.

We can't start design software architecture, or decide a software development model without knowing any requirements. 

Identifying requirements are important in software process. Any design activities should begin after requirements are understood. 

### Functional and Nonfunctional requirements

####Why it matters? 

In General:

* Functional Requirements specifies the functionality of the system.
* Non functional Requirements specifies the quality of the system.

If the system has been build does not do what stakeholders want (functionalities does not meet the stakeholders need), the system will be considered failure. 

If the system do what stakeholder want but without quality (low satisfiability, availability ...), for example, the system must send a warning message if the server is down with a latency of no greater than 5 minutes, and the stakeholders wouldn't happy if warning message was sent after few hours or days. 

#### Functional requirements
Functional requirements describes what a software system must do, the behavior of the system which is related to system's functionality. 

* Functional requirements are captured in `use cases`. 

* Functional requirements are supported by non functional requirements. 

#### Non functional requirements 
No functional requirements place constraints on how the system will do, specifies criteria that can be used to judge the software system. 

Non functional requirements may include:

* Accessibility
* Availability
* Documentation
* Disaster recovery
* Efficiency
* Extensibility
* Fault tolerance
* Maintainability
* Modifiability
* Open source
* Performance
* Price
* Privacy
* Portability
* Quality
* Reliability
* Response time
* Reusability
* Safety
* Scalability 
* Security
* Stability
* Supportability
* Testability
* Usability


#####Reference
[Software requirements](https://en.wikipedia.org/wiki/Software_requirements) 

[Non-functional requirement](https://en.wikipedia.org/wiki/Non-functional_requirement)

[Functional requirement](https://en.wikipedia.org/wiki/Functional_requirement)

