My Website & Blog
===

Using Github to host my Website and my Blog:
--
[My Domain][1] :            

> http://junjunguo.com

[My GitHub Domain][2] :     

> http://junjunguo.github.io

2015:

![2015](./image/readme/1.png)
![2015](./image/readme/2.png)
![2015](./image/readme/3.png)
![2015](./image/readme/4.png)
![2015](./image/readme/5.png)
![2015](./image/readme/6.png)

2014:

![2014](./image/readme/7.png)

Fun:

![2014](./image/readme/8.png)

Quotes:

![2014](./image/readme/9.png)

Photo:

![2014](./image/readme/10.png)
![2014](./image/readme/11.png)




[1]: http://junjunguo.com/
[2]: http://junjunguo.github.io